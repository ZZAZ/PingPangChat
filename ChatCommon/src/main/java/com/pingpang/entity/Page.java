package com.pingpang.entity;

import java.io.Serializable;

public class Page implements Serializable {
	
	private static final long serialVersionUID = -317884412373170597L;
	
	//信息查询分页使用
	private String page;
	private String limit;
	private String start;
	private String end;
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	
	
}
