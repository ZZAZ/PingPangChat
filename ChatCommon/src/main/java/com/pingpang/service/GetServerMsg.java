package com.pingpang.service;

public interface GetServerMsg {
	/**
	 * 获取webcsocket服务地址
	 * @return
	 */
	public String getServerIPMsg();
}
