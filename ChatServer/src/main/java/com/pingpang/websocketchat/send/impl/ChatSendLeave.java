package com.pingpang.websocketchat.send.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

/**
 * 离线操作
 * @author dell
 */
public class ChatSendLeave extends ChatSend{

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
		//ChannelManager.removeChannelByCode(message.getFrom().getUserCode());
		message.getFrom().setUserPassword("");
		message.setFrom(userService.getUser(message.getFrom()));
		message.setStatus("1");
		userMsgService.addMsg(message);
		ChannelManager.getChannel(message.getAccept().getUserCode())
		.writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
	}
}
