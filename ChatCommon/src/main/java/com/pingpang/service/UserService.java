package com.pingpang.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.pingpang.websocketchat.ChartUser;

public interface UserService {

	/**
	 * 更新map的某个字段
	 * 
	 * @param key
	 * @param mapKey
	 * @param value
	 */
	public void addHashMap(String key, String mapKey, Object value);

	/**
	 * 保存用户信息到hash表中
	 * 
	 * @param user
	 * @return
	 */
	public boolean addUserMap(ChartUser cu);

	/**
	 * 获取用户信息
	 * 
	 * @param user
	 * @return
	 */
	public ChartUser getUserMap(String userCode);

	/**
	 * 从数据库获取用户信息
	 * 
	 * @param userCode
	 * @return
	 */
	public ChartUser getUserByDB(String userCode);

	/**
	 * 获取所有在线用户
	 * 
	 * @return
	 */
	public Set<ChartUser> getAllUpUser();

	/**
	 * 添加用户
	 * 
	 * @param user
	 */
	public Map<String, Object> addUser(ChartUser user);

	/**
	 * 修改用户
	 * 
	 * @param user
	 */
	public void updateUser(ChartUser user);

	/**
	 * 获取用户
	 * 
	 * @param user
	 * @return
	 */
	public ChartUser getChartUser(String userCode);
	public ChartUser getChartOnUser(String userCode);
	public ChartUser getUser(ChartUser user);
	/*
	 * 移除用户
	 */
	public void removeChannelByCode(String userCode);
	/**
	 * 移除群组
	 * @param userCode
	 */
	public  void removeGroup(String userCode);
	/**
	 * 广播消息
	 * @param userCode
	 */
	public void sendAlertMsgByCode(String userCode,String msg);
	
	/**
	 * 当用户再客户端登录的时候后绑定服务端进行校验
	 * 
	 * @param user
	 * @param token
	 * @return
	 */
	public boolean addUserLoginToken(ChartUser user, String token);

	/**
	 * 从redis获取用户的数量
	 * 
	 * @return
	 */
	public int getAllRedisUser();

	/**
	 * 获取登录用户总数
	 * 
	 * @return
	 */
	public int getAllLoginUserCount();

	/**
	 * 获取用户总数
	 * 
	 * @param queryMap
	 * @return
	 */
	public int getAllUserCount(Map<String, String> queryMap);

	/**
	 * 获取所有用户数据
	 * 
	 * @param searchMap
	 * @return
	 */
	public Set<ChartUser> getAllUser(Map<String, String> searchMap);

	/**
	 * 获取带密码的数据
	 * 
	 * @param searchMap
	 * @return
	 */
	public Set<ChartUser> getRedisAllUser(Map<String, String> searchMap);

	/**
	 * 获取最近聊天得用户
	 * 
	 * @param cu
	 * @return
	 */
	public Set<ChartUser> getUserOldChat(ChartUser cu);

	// -------------非DAO数据，逻辑处理 开始--------------------------------------------

	/**
	 * 数据状状态同步
	 * 
	 * @param userCode
	 * @param userStatus
	 */
	public void dbDownUser(String userCode, String userStatus);

	/**
	 * 这里没用in操作，后续自行修改
	 * 
	 * @param userCode
	 * @param userStatus
	 */
	public void dbDownUser(Set<String> userCode, String userStatus);
	// -------------非DAO数据，逻辑处理 结束---------------------------------------------

	/**
	 * 获取用户注册数据
	 * 
	 * @param day
	 * @return
	 */
	public List<Map<String, String>> getUserRegsitCount(int day);

	/**
	 * 获取IP统计数据
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getIPCount();
}
