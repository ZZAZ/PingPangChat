package com.pingpang.websocketchat;

public class ChatType {
    
	//注销
	public static final String CANCEL="-1";
	
	//绑定上线
	public static final String BIND="1";
	
	//广播和提醒
	public static final String LEAVE="2";
	
	//单聊
	public static final String SINGLE="3";
	
	//群聊
	public static final String GROUP="4";
	
	//获取用户信息
	public static final String QUERY_USER="5";
	
	//获取群组信息
	public static final String QUERY_GROUP="6";
	
	//进群
	public static final String ADD_GROUP="7";
	
	//离开
	public static final String REMORE_GROUP="8";
	
	//获取历史聊天信息
	public static final String QUERY_OLD_MSG="9"; 
	
	//获取最近聊天人员
    public static final String QUERY_OLD_USER="10"; 
    
    //视频聊天 接收11 request refuse accept
    public static final String AUDIO_QUERY="11";
    
    //直播间
    public static final String AUDIO_LIVE="12";
    
    //图片特效
    public static final String AUDIO_MAGIC="13";
    
    //实时语音通话
    public static final String VOICE="14";
}
